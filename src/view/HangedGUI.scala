package view

import java.awt._
import javax.swing.{JButton, _}

import game.Hanged

import scala.List

/**
  * Created by lucch on 28/04/2017.
  */
class HangedGUI(var hanged: Hanged) extends JFrame("Hanged Game") {

  private val word: java.util.List[String] =  new java.util.ArrayList[String]
  private val sequence: java.util.List[JButton] =  new java.util.ArrayList[JButton]

  this.setSize(800, 400)
  this.setLayout(new BorderLayout)
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  hanged.nextWord().iterator.foreach(w => word.add(w))

  val livesPanel: JPanel = new JPanel
  val icon: ImageIcon = new ImageIcon("src/resources/image/lives.png")
  paintLives()
  this.add(livesPanel, BorderLayout.NORTH)

  val sequencePanel: JPanel = new JPanel
  sequencePanel.setLayout(new GridBagLayout)
  paintSequence()
  this.add(sequencePanel, BorderLayout.CENTER)

  val typesPanel: JPanel = new JPanel
  typesPanel.setPreferredSize(new Dimension(300, 70))
  this.paintTypes()
  this.add(typesPanel, BorderLayout.SOUTH)

  private def guessedType(indexes: Option[List[String]], buttonType: String) =  {
    indexes.isEmpty match {
      case false =>
        val ind = indexes.get
        for(i <- 0 until ind.length) sequence.get(Integer.valueOf(ind(i))).setText(buttonType)
        hanged.hadWon(word) match {
          case true =>
            val option = JOptionPane.showMessageDialog(this, "Congratulation, YOU WON!")
            System.exit(0)
          case _ =>
        }
      case _ =>
    }
  }

  private def paintLives() = {
    livesPanel.removeAll()
    livesPanel.revalidate()
    livesPanel.repaint()
    for (i <- 0 until hanged.lives()) {
      val label: JLabel = new JLabel(icon)
      livesPanel.add(label)
    }
  }

  private def paintTypes() ={
    val buttons: java.util.List[JButton] = new java.util.ArrayList[JButton]
    for (i <- 0 until hanged.allTypes.length) {
      buttons.add(i, new JButton(hanged.allTypes(i)))
      buttons.get(i).setEnabled(true)
      buttons.get(i).setBackground(Color.white)
      buttons.get(i).addActionListener(a =>{
        val indexes: Option[List[String]] = hanged.guessType(a.getActionCommand, word)
        paintLives()
        hanged.lives() match {
          case 0 =>
            var currentWord = ""
            word.stream().forEach(w => currentWord += w)
            val option = JOptionPane.showMessageDialog(this, "YOU LOSE!\n The word was: " + currentWord)
            System.exit(0)
          case _ =>
            this.guessedType(indexes, a.getActionCommand)
        }
        buttons.get(i).setEnabled(false)

      })
      typesPanel.add(buttons.get(i))
    }
  }

  private def paintSequence() = {
    for (i <- 0 until word.size()) {
      sequence.add(i, new JButton)
      sequence.get(i).setBackground(Color.white)
      sequence.get(i).setEnabled(false)
      sequence.get(i).setFont(new Font("sansserif", Font.BOLD, 36))
      sequence.get(i).setText("-")
      sequencePanel.add(sequence.get(i))
    }
  }

}