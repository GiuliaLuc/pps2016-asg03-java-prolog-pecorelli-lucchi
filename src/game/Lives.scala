package game

/**
  * Created by margherita on 28/04/17.
  */
trait Lives {
  def remainingLives: Int

  def decrement: Unit
}

case class LivesImpl(var lives: Int) extends Lives {
  def remainingLives = lives

  def decrement = lives-=1
}

object LivesImpl {
  private val DEFAULT_LIVES = 5

  implicit def nToLives(n: Int): LivesImpl = LivesImpl(n)

  def apply(): LivesImpl = new LivesImpl(DEFAULT_LIVES)
}
