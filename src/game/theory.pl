%All the types in the English alphabet
type("a",0).
type("b",0).
type("c",0).
type("d",0).
type("e",0).
type("f",0).
type("g",0).
type("h",0).
type("i",0).
type("j",0).
type("k",0).
type("l",0).
type("m",0).
type("n",0).
type("o",0).
type("p",0).
type("q",0).
type("r",0).
type("s",0).
type("t",0).
type("u",0).
type("v",0).
type("w",0).
type("x",0).
type("y",0).
type("z",0).

%all_types(-ListOfAllTypes)
all_types(L):- findall(X,type(X,_),L).

%types_used(-ListOfAllUsedTypes)
types_used(L):-findall(X,type(X,1),L).

%types_unused(-ListOfAllUnusedTypes)
types_unused(L):-findall(X,type(X,0),L).

%type_refresh(+TypeThatMustBeDisabled)
type_refresh(X):- retract(type(X,0)), assert(type(X,1)).

%sequence_to_array(+SequenceToConvertInArray,-ArrayOfSequenceTypes)
sequence_to_array(S,A):- atom_chars(S,A).

% search(+ElemToSearch,+List,-ElemIndexInList)
search_index(X,[X|_],0).
search_index(X,[_|Xs],C):- search_index(X,Xs,C1), C is C1+1.

% sublist(+Sequence,+UsedTypesList)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).
sublist([],L).
sublist([X|Xs],L):- search(X,L), sublist(Xs,L),!.



