package game

import scala.collection.JavaConverters._
import java.io.FileInputStream
import java.util.{Random, ResourceBundle}

import alice.tuprolog.{Struct, Term, Theory}

/**
  * Created by lucch on 29/04/2017.
  */
object HangedImpl {
  private val FILE_NAME = "src/game/theory.pl"
  private val INTRO_LABELS = ResourceBundle.getBundle("resources/Words")
  private val ENGINE = ScalaProlog.mkPrologEngine(new Theory(new FileInputStream(FILE_NAME)))
  private val N_OF_WORDS = 15
  private val LIVES = LivesImpl()
}

import HangedImpl._
case class HangedImpl() extends Hanged {

  override def allTypes = callPrologMethod("all_types(L)")

  override def unusedTypes = callPrologMethod("types_unused(L)")

  override def usedTypes = callPrologMethod("types_used(L)")

  private def callPrologMethod(prologMethod: String): List[String] = {
    val term = ScalaProlog.solveOneAndGetTerm(ENGINE, Term.createTerm(prologMethod), "L").asInstanceOf[Struct]
    termToList(term)
  }

  private def termToList(term: Struct): List[String] = {
    val iterator = term.listIterator()
    var typesList: List[String] = List()
    while (iterator.hasNext) {
      typesList = typesList :+ iterator.next().toString
    }
    typesList
  }

  private def refreshTypes(currentType: String) = ScalaProlog.solveWithSuccess(ENGINE, Term.createTerm(s"type_refresh('$currentType')"))

  override def nextWord(): List[String] = {
    val word = INTRO_LABELS.getString(new Random().nextInt(N_OF_WORDS).toString)
    callPrologMethod(s"sequence_to_array($word,L)")
  }

  private def checkLives() = LIVES.remainingLives match {
    case 1 =>
      LIVES.decrement
      println("YOU LOSE!!")
    case _ => LIVES.decrement
  }

  private def validType(currentType: String): Boolean = !usedTypes.contains(currentType)

  override def guessType(currentType: String, currentWord: java.util.List[String]) = {
    validType(currentType) match {
      case true =>
        refreshTypes(currentType)
        val stream = ScalaProlog.solveAllAndGetTerm(ENGINE, Term.createTerm(s"search_index('$currentType',${currentWord},L)"), "L")
        stream.isEmpty match {
          case true =>
            checkLives()
            None
          case false =>
            Option.apply(stream.toList.map(s => s.getTerm("L").toString))
        }
      case false =>
        println("ATTENTION: Type already used!")
        None
    }
  }

  override def hadWon(currentWord: java.util.List[String]): Boolean = {
    val used = asJavaCollection(usedTypes)
    ScalaProlog.solveWithSuccess(ENGINE, Term.createTerm(s"sublist($currentWord,$used)"))
  }

  override def lives(): Int = LIVES.remainingLives
}