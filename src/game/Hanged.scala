package game

/**
  * Created by margherita on 27/04/17.
  */
trait Hanged {
  def allTypes: List[String]

  def unusedTypes: List[String]

  def usedTypes: List[String]

  def nextWord(): List[String]

  def guessType(currentType: String, currentWord: java.util.List[String]): Option[List[String]]

  def hadWon(currentWord: java.util.List[String]): Boolean

  def lives(): Int
}
