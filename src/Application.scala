import game.HangedImpl
import view.HangedGUI

/**
  * Created by lucch on 28/04/2017.
  */
object Application extends App {
  val hanged = new HangedImpl
  new HangedGUI(hanged).setVisible(true)
}
