package test

import scala.collection.JavaConversions._
import game.HangedImpl

/**
  * Created by margherita on 27/04/17.
  */
object TestHanged extends App {
  val hangedImpl = new HangedImpl

  //random words
  println(hangedImpl.nextWord())
  println(hangedImpl.nextWord())
  println(hangedImpl.nextWord())
  println()

  val word = List("h","o","h","e")
  println(hangedImpl.usedTypes) //lista vuota
  println(hangedImpl.guessType("h",word)) //Some(List(0,2))
  println(hangedImpl.usedTypes) //h
  println(hangedImpl.guessType("t",word)) //None
  println(hangedImpl.usedTypes) //h,t
  println()

  println(hangedImpl.hadWon(word))//false
  hangedImpl.guessType("o",word)
  println(hangedImpl.hadWon(word))//false
  hangedImpl.guessType("e",word)
  println(hangedImpl.hadWon(word))//true
  println()

  hangedImpl.guessType("h",word) //ATTENTION: Type already used!
  println()

  //no more than 5 wrong choices (one already made while guessing "hohe" -> t)
  val word1 = List("e","a")
  hangedImpl.guessType("b",word1)
  hangedImpl.guessType("c",word1)
  hangedImpl.guessType("d",word1)
  hangedImpl.guessType("f",word1)
  hangedImpl.guessType("g",word1) //YOU LOSE!! System.exit
}
