package test

import game.LivesImpl

/**
  * Created by margherita on 28/04/17.
  */
object TestLives extends App {
  val lives: LivesImpl = 5
  assert(5 == lives.remainingLives)

  val lives2: LivesImpl = LivesImpl(3)
  assert(3 == lives2.remainingLives)

  val lives3: LivesImpl = LivesImpl()
  assert(5 == lives.remainingLives)
}
